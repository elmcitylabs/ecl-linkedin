#!/usr/bin/env/python

from setuptools import setup

setup(
    name = 'ecl_linkedin',
    version = '0.0.2',
    url = 'http://elmcitylabs.com',
    license = 'BSD',
    description = 'An easy wrapper for the Linkedin API',
    author = 'Dan Loewenherz, James Johnson',
    author_email = 'james@elmcitylabs.com',
    packages=["ecl_linkedin"],
    install_requires=["django>=1.3"],
)

