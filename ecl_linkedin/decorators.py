from functools import wraps

from django.http import HttpResponseRedirect
import logging
import linkedin

logger = logging.getLogger(__name__)

def linkedin_begin(fun):
    @wraps(fun)
    def k(request, *args, **kwargs):
        client = linkedin.LinkedIn(commas=True)
        data = client.generate_authorization_url()
        token = data['oauth_token']
        secret = data['oauth_token_secret']

        request.session['temporary_oauth_token'] = token
        request.session['temporary_oauth_secret'] = secret
        url = linkedin.LINKEDIN_BASE_URL + linkedin.LINKEDIN_OAUTH_AUTHORIZE
        return HttpResponseRedirect(url + '?oauth_token=' + token)

    return k

def linkedin_callback(fun):
    def k(request, *args, **kwargs):
        token = request.GET['oauth_token']
        verifier = request.GET['oauth_verifier']

        if token != request.session['temporary_oauth_token']:
            logger.warning("tokens don't match, do not move forward with authentication")

        secret = request.session['temporary_oauth_secret']
        client = linkedin.LinkedIn(token, secret, commas=True)
        data = client.generate_access_token(verifier)

        return fun(request, data, *args, **kwargs)
    return k


